import Skywakers from 'components/skywakers/skywakers';
import { RootAction } from 'features/redux/root-action';
import { RootState } from 'features/redux/root-reducer';
import { NextContext, NextSFC } from 'next';
import React from 'react';
import { Store } from 'redux';

const CounterPage: NextSFC<{}, {}, NextContext & { store: Store<RootState, RootAction> }> = () => {
    return (
        <div>
            <Skywakers />
        </div>);
};


export default CounterPage;
