import { createStyles, withStyles, WithStyles } from '@material-ui/core/styles';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import Typography from '@material-ui/core/Typography';
import { deletePerson } from 'features/counter/actions';
import { removeUser } from 'features/counter/selectors';
import { RootState } from 'features/redux/root-reducer';
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import FolderIcon from '@material-ui/icons/AccountCircle';
import DeleteIcon from '@material-ui/icons/Delete';

const styles = (theme: Theme) => createStyles({
    textColor: {
        color: theme.palette.primary.main,
    },
});

interface StateProps {
    skywakers: any;
}

interface DispatchProps {
    onDelete: () => any;
}

type Props = StateProps & DispatchProps & WithStyles<typeof styles>;

class Skywakers extends PureComponent<Props> {
    render() {
        const { classes, skywakers, onDelete} = this.props;
        return (
            <>
                <Typography variant="h3" className={classes.textColor}>Skywakers family</Typography>
                <List>
                    {
                        skywakers.map((item,index)=>{
                            return(
                                <ListItem key={`${index}`}>
                                    <ListItemAvatar>
                                        <Avatar>
                                            <FolderIcon />
                                        </Avatar>
                                    </ListItemAvatar>
                                    <ListItemText
                                        primary={item.name}
                                    />
                                    <ListItemSecondaryAction>
                                        <IconButton aria-label="Delete" onClick={()=> this.props.onDelete(index)}>
                                            <DeleteIcon />
                                        </IconButton>
                                    </ListItemSecondaryAction>
                                </ListItem>
                            );
                        })
                    }
                </List>
            </>
        );
    }
}

const mapStateToProps = (state: RootState) => {
    console.log(state);
    return {
        skywakers: state.counter.skywakers,
    };
};

const mapDispatchToProps = {
    onDelete: removeUser,
};

export default connect<StateProps, DispatchProps, {}, RootState>(mapStateToProps, mapDispatchToProps)
(withStyles(styles)(Skywakers));
