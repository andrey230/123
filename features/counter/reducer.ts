import { getType } from 'typesafe-actions';
import { RootAction } from '../redux';
import { decrementCounter, fetchCounterRequest, incrementCounter, deletePerson } from './actions';
import {getCounterFromSwapi} from './api';

export type CounterState = {
  readonly count: number;
  readonly isFetching: boolean;
  readonly skywakers: any;
};

export const initialState: CounterState = {
  count: 0,
  isFetching: false,
  skywakers:[
      {
        "name":"Luke Skywalker",
        "height":"172",
        "mass":"77",
        "hair_color":"blond",
        "skin_color":"fair",
        "eye_color":"blue",
        "birth_year":"19BBY","gender":"male","homeworld":"https://swapi.co/api/planets/1/","films":["https://swapi.co/api/films/2/","https://swapi.co/api/films/6/","https://swapi.co/api/films/3/","https://swapi.co/api/films/1/","https://swapi.co/api/films/7/"],"species":["https://swapi.co/api/species/1/"],"vehicles":["https://swapi.co/api/vehicles/14/","https://swapi.co/api/vehicles/30/"],"starships":["https://swapi.co/api/starships/12/","https://swapi.co/api/starships/22/"],"created":"2014-12-09T13:50:51.644000Z","edited":"2014-12-20T21:17:56.891000Z","url":"https://swapi.co/api/people/1/"},{"name":"Anakin Skywalker","height":"188","mass":"84","hair_color":"blond","skin_color":"fair","eye_color":"blue","birth_year":"41.9BBY","gender":"male","homeworld":"https://swapi.co/api/planets/1/","films":["https://swapi.co/api/films/5/","https://swapi.co/api/films/4/","https://swapi.co/api/films/6/"],"species":["https://swapi.co/api/species/1/"],"vehicles":["https://swapi.co/api/vehicles/44/","https://swapi.co/api/vehicles/46/"],"starships":["https://swapi.co/api/starships/59/","https://swapi.co/api/starships/65/","https://swapi.co/api/starships/39/"],"created":"2014-12-10T16:20:44.310000Z","edited":"2014-12-20T21:17:50.327000Z","url":"https://swapi.co/api/people/11/"},{"name":"Shmi Skywalker","height":"163","mass":"unknown","hair_color":"black","skin_color":"fair","eye_color":"brown","birth_year":"72BBY","gender":"female","homeworld":"https://swapi.co/api/planets/1/","films":["https://swapi.co/api/films/5/","https://swapi.co/api/films/4/"],"species":["https://swapi.co/api/species/1/"],"vehicles":[],"starships":[],"created":"2014-12-19T17:57:41.191000Z","edited":"2014-12-20T21:17:50.401000Z","url":"https://swapi.co/api/people/43/"}
    ],
  planets: getCounterFromSwapi(),
};

export default function (state: CounterState = initialState, action: RootAction): CounterState {
  switch (action.type) {
    case (getType(fetchCounterRequest.request)):
      return {
        ...state,
        isFetching: true,
      };
    case (getType(fetchCounterRequest.success)):
      return {
        ...state,
        count: action.payload,
        isFetching: false,
      };
    case (getType(fetchCounterRequest.failure)):
      return {
        ...state,
        isFetching: false,
      };
    case (getType(incrementCounter)):
      return {
        ...state,
        count: state.count + 1,
      };
    case (getType(deletePerson)):
      const index_of_user = action.payload;

      const new_skywakers = state.skywakers.filter((item,index)=>{
        if(index !== index_of_user){
          return item;
        }
      });


      return {
        ...state,
        skywakers: new_skywakers,
      };
    case (getType(decrementCounter)):
      return {
        ...state,
        count: state.count - 1,
      };
    default: {
      return state;
    }
  }
}
